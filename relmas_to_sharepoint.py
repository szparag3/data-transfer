#! python3

from selenium import webdriver

import time, pprint, koty, datetime, re, smtplib

try:
    print('===ReleaseMaster part===')
    RMbrowser = webdriver.Chrome()
    RMbrowser.implicitly_wait(30)
    RMbrowser.get('https://releasemaster.eng.vmware.com/reports/747/list/')
    RMbrowser.find_element_by_id('username').send_keys(koty.ad_us)
    RMbrowser.find_element_by_id('password').send_keys(koty.ad_pa)
    RMbrowser.find_element_by_id('password').submit()


    #show 100 elements in list
    RMbrowser.find_element_by_xpath("//select[@name='list_view_table_length']/option[@value='100']").click()



    #indexing column names 
    ######update CSS selector
    seleniumTh = RMbrowser.find_elements_by_xpath("//table[@id='list_view_table'] // th")
    Th = []

    for seleniumColumn in seleniumTh:
        Th = Th + [seleniumColumn.text]
    #sort table by GA
    print('sort')

    ######update CSS selector
    RMbrowser.find_element_by_xpath("(//table[@id='list_view_table'] // th)[{}]".format(Th.index('GA') + 1)).click()




    print('=Collecting the data from RM ...')
    rmData = []
    rmDataProducts = []

    #how many records
    ######update CSS selector
    seleniumTr = RMbrowser.find_elements_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row']")

    for row in range(1,len(seleniumTr)+1):
        rmLine = {}
        rmLine["release"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("Release") + 1)).text
        print(str(row) + ' - ' + rmLine["release"])
        rmLine["link"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}] / a".format(row,Th.index("Release") + 1)).get_attribute("href")
        rmDataProducts += [RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("Release") + 1)).text]
        rmLine["bu"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("BU") + 1)).text
        rmLine["ga"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GA") + 1)).text
        rmLine["quarter"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GA Fiscal Quarter") + 1)).text
        rmLine["brm"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GS Business Program Mgr") + 1)).text

        rmData.append(rmLine)

    #Closing clean up (browser)
    RMbrowser.quit()

    print('===sharePoint Part===')
    O365AAbrowser = webdriver.Chrome()
    O365AAbrowser.implicitly_wait(30)
    O365AAbrowser.get('http://login.vmware.com')
    O365AAbrowser.find_element_by_xpath("//p[contains(text(), 'Office 365')]").click()
    O365AAbrowser.switch_to_window(O365AAbrowser.window_handles[1])
    for s in range(30):
        time.sleep(1)
    O365AAbrowser.find_element_by_xpath("//div[contains(text(), ' SharePoint')]").click()
    O365AAbrowser.switch_to_window(O365AAbrowser.window_handles[2])
    O365AAbrowser.close()
    O365AAbrowser.switch_to_window(O365AAbrowser.window_handles[1])
    O365AAbrowser.get('https://onevmw.sharepoint.com/sites/gss-sharedservices/BSA/launchreadiness/Lists/lr/main.aspx')


    #colection column name
    spColSel = O365AAbrowser.find_elements_by_xpath("//thead//th")

    spCol = []
    for row in spColSel:
        spCol = spCol + [row.text.lower()]

    #create variable for data 
    spData = []
    spDataProducts = []
    #checking how many row
    spRows = O365AAbrowser.find_elements_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row']")

    print("=writing data ...")
    for row in range(len(spRows)):
        spLine = {}
        for column in range(len(spCol)-1):
            if spCol[column] != '':
                spLine[spCol[column]] = O365AAbrowser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text
                if spCol[column] == 'products':
                    print(str(row+1) + ' - ' + O365AAbrowser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text)
                    spDataProducts += [O365AAbrowser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text]
                    
        spData.append(spLine)
            
    print('Compare RM to SP data')

    def spUpdate(product):
        O365AAbrowser.find_element_by_link_text(product).click()
        for s in range(10):
            time.sleep(1)
        O365AAbrowser.find_element_by_xpath("//span[contains(text(), 'Edit')]").click()
        if bool(re.search(r'\d+/\d+/\d+', rmData[rmDataProducts.index(rmRow)]['ga'])):
            O365AAbrowser.find_element_by_xpath("//input[@title='GA']").clear()
            O365AAbrowser.find_element_by_xpath("//input[@title='GA']").send_keys(rmData[rmDataProducts.index(product)]['ga'])
            O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").clear()
            O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").send_keys("0")
        else:
            O365AAbrowser.find_element_by_xpath("//input[@title='GA']").clear()
            O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").clear()
            O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").send_keys("9")
        O365AAbrowser.find_element_by_xpath("//input[@title='Quarter']").clear()
        O365AAbrowser.find_element_by_xpath("//input[@title='Quarter']").send_keys(rmData[rmDataProducts.index(product)]['quarter'])    
        O365AAbrowser.find_element_by_xpath("//input[@title='BU']").clear()
        O365AAbrowser.find_element_by_xpath("//input[@title='BU']").send_keys(rmData[rmDataProducts.index(product)]['bu'])
        O365AAbrowser.find_element_by_xpath("//input[@title='Main Contact (GS)']").clear()
        O365AAbrowser.find_element_by_xpath("//input[@title='Main Contact (GS)']").send_keys(rmData[rmDataProducts.index(product)]['brm'])


        
        if not O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_1").is_selected():
            O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_1").click()
        O365AAbrowser.find_element_by_xpath("//input[@name='ctl00$ctl40$g_a187db30_6a84_4792_a01d_0c11439f997b$ctl00$toolBarTbl$RightRptControls$ctl00$ctl00$diidIOSaveItem']").click()
     
        
        

    for rmRow in rmDataProducts:
        if rmRow in spDataProducts:
            print('on the list - {}'.format(rmRow))
            if bool(re.search(r'\d+/\d+/\d+', rmData[rmDataProducts.index(rmRow)]['ga'])):
                print("good format")
                if spData[spDataProducts.index(rmRow)]['ga'] == '':
                    print("date need update")
                    spUpdate(rmRow)
                    continue    
                rmRowGA = datetime.datetime.strptime(rmData[rmDataProducts.index(rmRow)]['ga'], '%m/%d/%Y')
                spRowGA = datetime.datetime.strptime(spData[spDataProducts.index(rmRow)]['ga'], '%m/%d/%Y')
                if rmRowGA != spRowGA:
                    print("date need update")
                    spUpdate(rmRow)
                    continue
            else:
                print("wrong format")
            if rmData[rmDataProducts.index(rmRow)]['brm'] != spData[spDataProducts.index(rmRow)]['main contact (gs)']:
                print("BRM update")
                spUpdate(rmRow)
                continue
                                                                                                                   
            if rmData[rmDataProducts.index(rmRow)]['bu'] != spData[spDataProducts.index(rmRow)]['bu']:
                print("BU update ")
                spUpdate(rmRow)
                continue
            if rmData[rmDataProducts.index(rmRow)]['quarter'] != spData[spDataProducts.index(rmRow)]['quarter']:
                print("quarter update")
                spUpdate(rmRow)
                continue

                                    
        else:
            print('New product - {}'.format(rmRow))
            O365AAbrowser.find_element_by_link_text('new item').click()
            O365AAbrowser.find_element_by_xpath("//input[@title='Products Required Field']").send_keys(rmData[rmDataProducts.index(rmRow)]['release'])
            if bool(re.search(r'\d+/\d+/\d+', rmData[rmDataProducts.index(rmRow)]['ga'])):
                O365AAbrowser.find_element_by_xpath("//input[@title='GA']").send_keys(rmData[rmDataProducts.index(rmRow)]['ga'])
            else:
                O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").clear()
                O365AAbrowser.find_element_by_xpath("//input[@title='Sort group']").send_keys("9")
            O365AAbrowser.find_element_by_xpath("//input[@title='Quarter']").send_keys(rmData[rmDataProducts.index(rmRow)]['quarter'])
            O365AAbrowser.find_element_by_xpath("//input[@title='BU']").send_keys(rmData[rmDataProducts.index(rmRow)]['bu'])
            O365AAbrowser.find_element_by_xpath("//input[@title='Main Contact (GS)']").send_keys(rmData[rmDataProducts.index(rmRow)]['brm'])
            if not O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_1").is_selected():
                O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_1").click()
            if O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_0").is_selected():
                O365AAbrowser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_0").click()
            O365AAbrowser.find_element_by_xpath("//input[@id='Direct_x0020_Link_0e0820d9-87fe-4037-988c-4ebadbb76b26_$UrlFieldUrl']").clear()
            O365AAbrowser.find_element_by_xpath("//input[@id='Direct_x0020_Link_0e0820d9-87fe-4037-988c-4ebadbb76b26_$UrlFieldUrl']").send_keys(rmData[rmDataProducts.index(rmRow)]['link'])
            O365AAbrowser.find_element_by_xpath("//input[@name='ctl00$ctl40$g_05a14e3d_3737_43ff_9616_4daad24cfbcf$ctl00$toolBarTbl$RightRptControls$ctl00$ctl00$diidIOSaveItem']").click()

    #Remove SP records which are not in RM
    for spRow in spDataProducts:
        if "ReleaseMaster" in spData[spDataProducts.index(spRow)]['data source']:
            if spRow not in rmDataProducts:
                spRowGA = datetime.datetime.today() + datetime.timedelta(1) # include to cover all products without date data
                if spData[spDataProducts.index(spRow)]['ga'] != '':
                    spRowGA = datetime.datetime.strptime(spData[spDataProducts.index(spRow)]['ga'], '%m/%d/%Y')#option to do not touch relased products
                    if spRowGA >= datetime.datetime.today():
                        print("remove - "+spRow)
                        O365AAbrowser.find_element_by_link_text(spRow).click()
                        for s in range(10):
                            time.sleep(1)
                        O365AAbrowser.find_element_by_xpath("//span[contains(text(), 'Delete Item')]").click()
                        O365AAbrowser.switch_to.alert.accept()
                

    O365AAbrowser.find_element_by_link_text('edit').click()
    #Closing clean up (browser)
    O365AAbrowser.quit()  

except:
    print('HELP -- ERROR ERROR ERROR')
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.login('pnawrocki1983@gmail.com', 'xxxx')
    smtpobj.sendmail('pnawrocki1983@gmail.com', 'szparag3@gmail.com', 'Subject: {0} AUTOMATION ERROR\n{0} AUTOMATION ERROR\nERROR'.format('RM to SP'))
    smtpobj.quit()
    input()