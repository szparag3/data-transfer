#! python3
from selenium import webdriver
if __name__ == "__main__":
    import os
    os.path.abspath(os.curdir)
    os.chdir("..")
    WorkingDir = os.path.abspath(os.curdir)
    import sys
    sys.path.append(WorkingDir)
else:
    pass
import koty

def login():
    SFDCbrowser = webdriver.Chrome()
    SFDCbrowser.implicitly_wait(30)
    SFDCbrowser.get('https://login.salesforce.com')
    SFDCbrowser.find_element_by_id('username').send_keys(koty.sfdc_us)
    SFDCbrowser.find_element_by_id('password').send_keys(koty.sfdc_pa)
    SFDCbrowser.find_element_by_id('password').submit()
 
    return SFDCbrowser

def closeTheBrowser(driver):
    driver.quit()



def search(driver, search):
    driver.find_element_by_link_text('Advanced Search...').click()
    driver.find_element_by_css_selector('input#str[title="Enter search keywords"]').send_keys(search)
    driver.find_element_by_css_selector('input#str[title="Enter search keywords"]').submit()

def openCase(driver, caseNum):
    driver.get('https://vmware.my.salesforce.com/apex/GSS_SearchObjectPage?id=00580000003k8Iq')
    driver.find_element_by_id("j_id0:j_id3:pblock:j_id4:j_id5:j_id6:idSearch").send_keys(caseNum)
    driver.find_element_by_id("j_id0:j_id3:pblock:j_id4:j_id14:j_id15").click()
    link = driver.find_element_by_link_text(caseNum).get_attribute("href")
    driver.get(link)

        
    
