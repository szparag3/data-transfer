#! python3

from selenium import webdriver
import time, pprint, datetime, smtplib, koty
from inputs import sfdc_dump


print('SFDC Browser start')
SFDCbrowser = webdriver.Chrome()
SFDCbrowser.implicitly_wait(30)
SFDCbrowser.get('https://login.salesforce.com')
SFDCbrowser.find_element_by_id('username').send_keys(koty.sfdc_us)
SFDCbrowser.find_element_by_id('password').send_keys(koty.sfdc_pa)
SFDCbrowser.find_element_by_id('password').submit()


if len(SFDCbrowser.find_elements_by_link_text("Reports")) == 0:
    SFDCbrowser.find_element_by_id("tsidLabel").click()
    SFDCbrowser.find_element_by_link_text("Change Management").click()




SFDCbrowser.get('https://vmware.my.salesforce.com/00O34000006UBCQ')
SFDCbrowser.find_element_by_name("run").click()

for s in range(10):
    time.sleep(1)

print('=Collecting the data from SFDC ...')
#colection column name
seleniumThSfdc = SFDCbrowser.find_elements_by_xpath("//div[@id='fchArea']//tr[@id='headerRow_0']//th")
thSfdc = []
for seleniumColumnSfdc in seleniumThSfdc:
    thSfdc = thSfdc + [seleniumColumnSfdc.text.lower()]

seleniumTrSfdc = SFDCbrowser.find_elements_by_xpath("//div[@id='fchArea']//tr[@class='even']")

sfdcCase = {}

for rowSfdc in range(len(seleniumTrSfdc)):
    for columnSfdc in range(len(thSfdc)):
        if thSfdc[columnSfdc] == "case number":
            caseNumSfdc = SFDCbrowser.find_element_by_xpath("//div[@id='fchArea']//tr[@class='even'][{}+1]/td[{}+1]".format(rowSfdc,columnSfdc)).text
            sfdcCase[caseNumSfdc] = {}
            
            break
    for columnSfdc in range(len(thSfdc)):
        if thSfdc[columnSfdc] == "case number":
            continue
        columnHeaderSfdc = thSfdc[columnSfdc]
        sfdcCase[caseNumSfdc][columnHeaderSfdc] = SFDCbrowser.find_element_by_xpath("//div[@id='fchArea']//tr[@class='even'][{}+1]/td[{}+1]".format(rowSfdc,columnSfdc)).text
        if thSfdc[columnSfdc] == "subject":
            print(rowSfdc + 1,"-",caseNumSfdc,"-",sfdcCase[caseNumSfdc][columnHeaderSfdc])
SFDCbrowser.quit()



print('SP Browser start')
O365browser = webdriver.Chrome()
O365browser.implicitly_wait(30)
O365browser.get('http://login.vmware.com')
O365browser.find_element_by_xpath("//p[contains(text(), 'Office 365')]").click()
O365browser.switch_to_window(O365browser.window_handles[1])
O365browser.find_element_by_xpath("//span[contains(text(), ' SharePoint')]").click()
O365browser.switch_to_window(O365browser.window_handles[2])
O365browser.close()
O365browser.switch_to_window(O365browser.window_handles[1])
O365browser.get('https://onevmw.sharepoint.com/sites/gss-sharedservices/BSA/launchreadiness/Lists/lr_cases/PersonalViews.aspx?PageView=Personal&ShowWebPart={A257F1B9-6E1E-415F-9A74-1B593C0EAFDA}')

print('=Collecting the data from SP ...')
#colection column name
seleniumThSp = O365browser.find_elements_by_xpath("//thead//th")
thSp = []

for seleniumColumnSp in seleniumThSp:
    thSp = thSp + [seleniumColumnSp.text.lower()]



#create variable for data 
spCase = {}

#checking how many row
seleniumTrSp = O365browser.find_elements_by_xpath("//table[@summary='LR Cases']//tbody//tr[@role='row']")

print("=writing data ...")
for rowSp in range(len(seleniumTrSp)):
    for columnSp in range(len(thSp)):
        if thSp[columnSp] != '':
            if thSp[columnSp] == 'case number':
                caseNumSp = O365browser.find_element_by_xpath("//table[@summary='LR Cases']//tbody//tr[@role='row'][{}]//td[{}]".format(rowSp+1,columnSp+1)).text
                spCase[caseNumSp] = {}
                print(caseNumSp)

                break
    for columnSp in range(len(thSp)):
        if thSp[columnSp] == "case number":
            continue
        columnHeaderSp = thSp[columnSp]
        ### problem with xpath
        spCase[caseNumSp][columnHeaderSp] = O365browser.find_element_by_xpath("//div[@id='fchArea']//tr[@class='even'][{}+1]/td[{}+1]".format(rowSfdc,columnSfdc)).text
        if thSp[columnSp] == "subject":
            print(rowSp + 1,"-",caseNumSp,"-",spCase[caseNumSp][columnHeaderSp])
O365browser.quit()
