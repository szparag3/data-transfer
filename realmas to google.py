#! python3

from selenium import webdriver


from oauth2client.service_account import ServiceAccountCredentials
import time, gspread, pprint, koty, datetime, pygsheets, re, smtplib

try:
    RMbrowser = webdriver.Chrome()
    RMbrowser.implicitly_wait(30)
    RMbrowser.get('https://releasemaster.eng.vmware.com/reports/747/list/')
    RMbrowser.find_element_by_id('username').send_keys(koty.ad_us)
    RMbrowser.find_element_by_id('password').send_keys(koty.ad_pa)
    RMbrowser.find_element_by_id('password').submit()

    #show 100 elements in list
    RMbrowser.find_element_by_xpath("//select[@name='list_view_table_length']/option[@value='100']").click()


    
    #indexing column names
    seleniumTh = RMbrowser.find_elements_by_xpath("//table[@id='list_view_table'] // th")
    Th = []

    for seleniumColumn in seleniumTh:
        Th = Th + [seleniumColumn.text]

    #sort table by GA
    print('sort')


    RMbrowser.find_element_by_xpath("(//table[@id='list_view_table'] // th)[{}]".format(Th.index('GA') + 1)).click()




    print('Collecting the data ...')
    rmData = []
    rmDataProducts = []

    #how many records
    seleniumTr = RMbrowser.find_elements_by_css_selector("table#list_view_table > tbody > tr")

    seleniumTr = RMbrowser.find_elements_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row']")

    for row in range(1,len(seleniumTr)+1):
        rmLine = {}
        rmLine["release"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("Release") + 1)).text
        print(str(row) + ' - ' + rmLine["release"])
        rmDataProducts += [RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("Release") + 1)).text]
        rmLine["bu"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("BU") + 1)).text
        rmLine["ga"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GA") + 1)).text
        rmLine["quarter"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GA Fiscal Quarter") + 1)).text
        rmLine["brm"] = RMbrowser.find_element_by_xpath("//table[@id='list_view_table'] / tbody / tr[@role='row'][{0}] / td[{1}]".format(row,Th.index("GS Business Program Mgr") + 1)).text

        rmData.append(rmLine)

    #Closing clean up (browser)
    RMbrowser.quit()



    print('google spreasheet part')

    #start gspread

    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('inputs/gspread-vmw-c79093a07c54.json', scope)

    gsc = gspread.authorize(credentials)
    gswks = gsc.open_by_url('https://docs.google.com/spreadsheets/d/1ZOegpSYLGEDx4huP7lvtaXmI6IhaFv53pNXiUP5jaCo')
    gssh = gswks.worksheet("Release Calendar")

    #start pysheets
    pgsc = pygsheets.authorize(service_file='inputs/pygsheets-vmw-81fdca7a86cc.json')
    pgwks = pgsc.open_by_url('https://docs.google.com/spreadsheets/d/1ZOegpSYLGEDx4huP7lvtaXmI6IhaFv53pNXiUP5jaCo')

    pgsh = pgwks.worksheet_by_title("Release Calendar")

    print("spreadsheet data colection ...")
    col = 1
    while gssh.cell(1,col).value != "Start date:":
        lastColumn = col + 2
        col += 1

    today = datetime.datetime.now() 
    currentWeek = today - datetime.timedelta(days=today.weekday())

    #update current week
    gssh.update_cell(1, lastColumn, currentWeek.strftime('%b/%d/%Y'))


    #fields
    productsCol = gssh.find("Products").col
    buCol = gssh.find("BU").col
    maincontactCol = gssh.find("Main Contact (PPM/BSA)").col
    gaCol = gssh.find("GA").col
    status = gssh.find("Status").col
    sys = gssh.find("#System log").col

    relList = []
    relListDel = []
    for i in rmData:
        relList.append(i['release'])
        relListDel.append(i['release'])

    print("Update")
    p = re.compile('\d+/\d+/\d+')
    lastCount = 0
    for i in range(3,gssh.row_count):
        if i%15 == 0:
            print(round(i*100/gssh.row_count),'%')
        if gssh.cell(i,1).value in relList:
            if gssh.cell(i, buCol).value != rmData[relList.index(gssh.cell(i,1).value)]['bu']:
                print('up row {} bu'.format(i))
                gssh.update_cell(i, buCol, rmData[relList.index(gssh.cell(i,1).value)]['bu'])
            if gssh.cell(i, maincontactCol).value != rmData[relList.index(gssh.cell(i,1).value)]['brm']:
                print('up row {} brm'.format(i))
                gssh.update_cell(i, maincontactCol, rmData[relList.index(gssh.cell(i,1).value)]['brm'])
                
            gaFormat = rmData[relList.index(gssh.cell(i,1).value)]['ga']
            if p.match(gaFormat):
                gaFormat = datetime.datetime.strptime(gaFormat, '%m/%d/%Y')
                gaFormat = gaFormat.strftime('%b/%d/%Y')
                
            if gssh.cell(i, gaCol).value != gaFormat:
                print('up row {} ga - {}'.format(i, gaFormat))
                gssh.update_cell(i, gaCol, gaFormat)
            gssh.update_cell(i, sys, 'RelMaster {}'.format(today.strftime('%b/%d/%Y')))
            if gssh.cell(i,1).value in relListDel:
                relListDel.remove(gssh.cell(i,1).value)
            else:
                gssh.update_cell(i, sys, 'DUPLICATE RelMaster {}'.format(today.strftime('%b/%d/%Y')))

        #check last row
        
        if gssh.cell(i,1).value == '':
            lastCount += 1
        if lastCount == 1:
            lastRow = i
        
    print('100 %')                    
    print("new lines")
    for j in relListDel:
        print(j)
        gssh.update_cell(lastRow, 1, rmData[relList.index(j)]['release'])
        gssh.update_cell(lastRow, buCol, rmData[relList.index(j)]['bu'])
        gssh.update_cell(lastRow, maincontactCol, rmData[relList.index(j)]['brm'])

        gaFormat = rmData[relList.index(j)]['ga']
        if p.match(gaFormat):
            gaFormat = datetime.datetime.strptime(gaFormat, '%m/%d/%Y')
            gaFormat = gaFormat.strftime('%b/%d/%Y')
        gssh.update_cell(lastRow, gaCol, gaFormat)
        gssh.update_cell(lastRow, status, 'Not started')
        gssh.update_cell(lastRow, sys, 'RelMaster {}'.format(today.strftime('%b/%d/%Y')))
        lastRow += 1

        
        
    #Closing clean up (browser)
    RMbrowser.quit()

    print('Logs...')
    wksLog = gsc.open_by_url('https://docs.google.com/spreadsheets/d/1pI19aUYY83qNfrYp_4bprRxd2wheznpXqTt_Skp8CiA')
    shLog = wksLog.worksheet("logs")
    shLog.add_rows(1)
    shLog.update_cell(shLog.row_count, 1, datetime.datetime.now().strftime('%Y/%b/%d %H:%M'))
    shLog.update_cell(shLog.row_count, 2, 'GA')
    print('END') 

except:
    print('HELP -- ERROR ERROR ERROR')
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.login('pnawrocki1983@gmail.com', 'xxxx')
    smtpobj.sendmail('pnawrocki1983@gmail.com', 'szparag3@gmail.com', 'Subject: {0} AUTOMATION ERROR\n{0} AUTOMATION ERROR\nERROR'.format('GA'))
    smtpobj.quit()
    input()
    