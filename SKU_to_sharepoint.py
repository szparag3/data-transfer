#! python3

from selenium import webdriver

import time, pprint, koty, datetime, re, smtplib

try:
    print('===SKU Roadmap - 1 - Final GA===')
    spbrowser = webdriver.Chrome()
    spbrowser.implicitly_wait(30)
    spbrowser.get('http://login.vmware.com')
    spbrowser.find_element_by_xpath("//p[contains(text(), 'Office 365')]").click()
    spbrowser.switch_to_window(spbrowser.window_handles[1])
    spbrowser.find_element_by_xpath("//span[contains(text(), ' SharePoint')]").click()
    spbrowser.switch_to_window(spbrowser.window_handles[2])
    spbrowser.close()
    spbrowser.switch_to_window(spbrowser.window_handles[1])
    spbrowser.get('https://vmshare.vmware.com/product-operations/_layouts/15/ViewEdit.aspx?List=b4b4c5b6-b9dc-4500-8bcb-f42939c3177f&View=%7BCBDDDAAB-B1B0-406F-BCA2-408AA4032A1F%7D&Source=https%3A%2F%2Fvmshare%2Evmware%2Ecom%2Fproduct-operations%2FLists%2FMaster%2520List%2FPersonalViews%2Easpx%3FPageView%3DPersonal%26ShowWebPart%3D%7BCBDDDAAB-B1B0-406F-BCA2-408AA4032A1F%7D')

    spbrowser.find_element_by_xpath("//select[@name='FieldPicker1'] // option[contains(text(), 'Final GA')]").click()
    spbrowser.find_element_by_xpath("//tbody[@id='tbodyViewFilter']//tbody//input[@type='text']").clear()
    spbrowser.find_element_by_xpath("//tbody[@id='tbodyViewFilter']//tbody//input[@type='text']").send_keys(datetime.datetime.today().strftime("%m/%d/%Y"))

    spbrowser.find_element_by_xpath("//select[@name='FieldPicker2'] // option[contains(text(), 'Final GA')]").click()
    spbrowser.find_element_by_xpath("//tbody[@id='tbodyViewFilter']//tbody[@id='tbBlock1']//input[@type='text']").clear()
    spbrowser.find_element_by_xpath("//tbody[@id='tbodyViewFilter']//tbody[@id='tbBlock1']//input[@type='text']").send_keys((datetime.datetime.now() + datetime.timedelta(90)).strftime("%m/%d/%Y"))
    spbrowser.find_element_by_id("onetidSaveItem").click()

    #colection column name
    spSkuColSel = spbrowser.find_elements_by_xpath("//thead//th")

    spSkuCol = []
    for row in spSkuColSel:
        spSkuCol = spSkuCol + [row.text.lower()]

    #create variable for data 
    spFinData = []
    spFinDataProducts = []
    #checking how many row
    spRows = spbrowser.find_elements_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr")

    print("=writing data ...")
    for row in range(len(spRows)):
        if spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,spSkuCol.index('project status')+1)) != 'Cancelled':
            spLine = {}
            for column in range(len(spSkuCol)-1):
                if spSkuCol[column] != '':
                    spLine[spSkuCol[column]] = spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text
                    if spSkuCol[column] == 'offering/proposal':
                        print(str(row+1) + ' - ' + spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text)
                        spFinDataProducts += [spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text]
                        spLine['link'] = spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]//a".format(row+1,column+1)).get_attribute("href")
                        
        spFinData.append(spLine)
        
    print('===SKU Roadmap - 2 - Proposed GA===')
    spbrowser.get('https://vmshare.vmware.com/product-operations/_layouts/15/ViewEdit.aspx?List=b4b4c5b6-b9dc-4500-8bcb-f42939c3177f&View=%7BCBDDDAAB-B1B0-406F-BCA2-408AA4032A1F%7D&Source=https%3A%2F%2Fvmshare%2Evmware%2Ecom%2Fproduct-operations%2FLists%2FMaster%2520List%2FPersonalViews%2Easpx%3FPageView%3DPersonal%26ShowWebPart%3D%7BCBDDDAAB-B1B0-406F-BCA2-408AA4032A1F%7D')


    spbrowser.find_element_by_xpath("//select[@name='FieldPicker1'] // option[contains(text(), 'Proposed GA')]").click()
    spbrowser.find_element_by_xpath("//select[@name='FieldPicker2'] // option[contains(text(), 'Proposed GA')]").click()
    spbrowser.find_element_by_id("onetidSaveItem").click()

    #create variable for data 
    spPropData = []
    spPropDataProducts = []
    #checking how many row
    spPropRows = spbrowser.find_elements_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr")

    print("=writing data ...")
    for row in range(len(spRows)):
        if spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,spSkuCol.index('project status')+1)) != 'Cancelled':
            if spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,(spSkuCol.index('final ga')+1))).text == '':
                spLine = {}
                for column in range(len(spSkuCol)-1):
                    if spSkuCol[column] != '':
                        spLine[spSkuCol[column]] = spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text
                        if spSkuCol[column] == 'offering/proposal':
                            print(str(row+1) + ' - ' + spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text)
                            spPropDataProducts += [spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]".format(row+1,column+1)).text]
                            spLine['link'] = spbrowser.find_element_by_xpath("//table[@summary='Master List Active Dashboard Master List for active dashboard']/tbody/tr[{}]/td[{}]//a".format(row+1,column+1)).get_attribute("href")
                        
            spPropData.append(spLine)

    spbrowser.quit()

    #Join data
    spJoinData = spFinData + spPropData
    spJoinDataProducts = spFinDataProducts + spPropDataProducts

    print('===BSA SP - 3===')
    O365browser = webdriver.Chrome()
    O365browser.implicitly_wait(30)
    O365browser.get('http://login.vmware.com')
    O365browser.find_element_by_xpath("//p[contains(text(), 'Office 365')]").click()
    O365browser.switch_to_window(O365browser.window_handles[1])
    O365browser.find_element_by_xpath("//span[contains(text(), ' SharePoint')]").click()
    O365browser.switch_to_window(O365browser.window_handles[2])
    O365browser.close()
    O365browser.switch_to_window(O365browser.window_handles[1])
    O365browser.get('https://onevmw.sharepoint.com/sites/gss-sharedservices/BSA/launchreadiness/Lists/lr/AllItems.aspx')


    #colection column name
    spColSel = O365browser.find_elements_by_xpath("//thead//th")

    spCol = []
    for row in spColSel:
        spCol = spCol + [row.text.lower()]

    #create variable for data 
    spData = []
    spDataProducts = []
    #checking how many row
    spRows = O365browser.find_elements_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row']")

    print("=writing data ...")
    for row in range(len(spRows)):
        spLine = {}
        for column in range(len(spCol)-1):
            if spCol[column] != '':
                spLine[spCol[column]] = O365browser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text
                if spCol[column] == 'products':
                    print(str(row+1) + ' - ' + O365browser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text)
                    spDataProducts += [O365browser.find_element_by_xpath("//table[@summary='LR Calendar']//tbody//tr[@role='row'][{}]//td[{}]".format(row+1,column+1)).text]
                    
        spData.append(spLine)
            
    print('Compare SKU to SP data')

    def spUpdate(product):
        O365browser.find_element_by_link_text(product).click()
        O365browser.find_element_by_xpath("//span[contains(text(), 'Edit')]").click()
        O365browser.find_element_by_xpath("//input[@title='GA']").clear()
        if spJoinData[spJoinDataProducts.index(product)]['final ga'] == '':
            O365browser.find_element_by_xpath("//input[@title='GA']").send_keys(spJoinData[spJoinDataProducts.index(product)]['proposed ga'])
        else:
            O365browser.find_element_by_xpath("//input[@title='GA']").send_keys(spJoinData[spJoinDataProducts.index(product)]['final ga'])
        O365browser.find_element_by_xpath("//input[@title='Quarter']").clear()
        O365browser.find_element_by_xpath("//input[@title='Quarter']").send_keys(spJoinData[spJoinDataProducts.index(product)]['ga qtr'])    
        O365browser.find_element_by_xpath("//input[@title='BU']").clear()
        O365browser.find_element_by_xpath("//input[@title='BU']").send_keys('SKU')



        
        if not O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_2").is_selected():
            O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_2").click()
        O365browser.find_element_by_xpath("//input[@name='ctl00$ctl40$g_a187db30_6a84_4792_a01d_0c11439f997b$ctl00$toolBarTbl$RightRptControls$ctl00$ctl00$diidIOSaveItem']").click()
     
        
        

    for spJoinRow in spJoinDataProducts:
        if spJoinRow in spDataProducts:
            print('on the list - {}'.format(spJoinRow))
            if spJoinData[spJoinDataProducts.index(spJoinRow)]['final ga'] == '':
                spJoinRowGA = datetime.datetime.strptime(spJoinData[spJoinDataProducts.index(spJoinRow)]['proposed ga'], '%m/%d/%Y')
            else:
                spJoinRowGA = datetime.datetime.strptime(spJoinData[spJoinDataProducts.index(spJoinRow)]['final ga'], '%m/%d/%Y')
            spRowGA = datetime.datetime.strptime(spData[spDataProducts.index(spJoinRow)]['ga'], '%m/%d/%Y')
            if spJoinRowGA != spRowGA:
                print("date need update")
                spUpdate(spJoinRow)
                continue
                                                                                                                                  
        else:
            print('New product - {}'.format(spJoinRow))
            O365browser.find_element_by_link_text('new item').click()
            O365browser.find_element_by_xpath("//input[@title='Products Required Field']").send_keys(spJoinData[spJoinDataProducts.index(spJoinRow)]['offering/proposal'])
            if spJoinData[spJoinDataProducts.index(spJoinRow)]['final ga'] == '':
                O365browser.find_element_by_xpath("//input[@title='GA']").send_keys(spJoinData[spJoinDataProducts.index(spJoinRow)]['proposed ga'])
            else:
                O365browser.find_element_by_xpath("//input[@title='GA']").send_keys(spJoinData[spJoinDataProducts.index(spJoinRow)]['final ga'])
            O365browser.find_element_by_xpath("//input[@title='Quarter']").send_keys(spJoinData[spJoinDataProducts.index(spJoinRow)]['ga qtr'])    
            O365browser.find_element_by_xpath("//input[@title='BU']").send_keys('SKU')
            

            if not O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_2").is_selected():
                O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_2").click()
            if O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_0").is_selected():
                O365browser.find_element_by_id("Data_x0020_Source_3b262cda-2166-4296-9233-c20ec5fdfc9a_MultiChoiceOption_0").click()
                O365browser.find_element_by_xpath("//input[@id='Direct_x0020_Link_0e0820d9-87fe-4037-988c-4ebadbb76b26_$UrlFieldUrl']").clear()
                O365browser.find_element_by_xpath("//input[@id='Direct_x0020_Link_0e0820d9-87fe-4037-988c-4ebadbb76b26_$UrlFieldUrl']").send_keys(spJoinData[spJoinDataProducts.index(spJoinRow)]['link'])

            O365browser.find_element_by_xpath("//input[@name='ctl00$ctl40$g_05a14e3d_3737_43ff_9616_4daad24cfbcf$ctl00$toolBarTbl$RightRptControls$ctl00$ctl00$diidIOSaveItem']").click()

    #Remove SP records which are not in RM
    for spRow in spDataProducts:
        if "SKU Roadmap" in spData[spDataProducts.index(spRow)]['data source']:
            if spRow not in spJoinDataProducts:
                spRowGA = datetime.datetime.today() + datetime.timedelta(1) # include to cover all products without date data
                if spData[spDataProducts.index(spRow)]['ga'] != '':
                    spRowGA = datetime.datetime.strptime(spData[spDataProducts.index(spRow)]['ga'], '%m/%d/%Y')#option to do not touch relased products
                if spRowGA >= datetime.datetime.today():
                    print("remove - "+spRow)
                    O365browser.find_element_by_link_text(spRow).click()
                    for s in range(10):
                        time.sleep(1)
                    O365browser.find_element_by_xpath("//span[contains(text(), 'Delete Item')]").click()
                    O365browser.switch_to.alert.accept()
                

    O365browser.find_element_by_link_text('edit').click()
    #Closing clean up (browser)
    O365browser.quit()

except:
    print('HELP -- ERROR ERROR ERROR')
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.login('pnawrocki1983@gmail.com', 'xxxx')
    smtpobj.sendmail('pnawrocki1983@gmail.com', 'szparag3@gmail.com', 'Subject: {0} AUTOMATION ERROR\n{0} AUTOMATION ERROR\nERROR'.format('SKU to SP'))
    smtpobj.quit()
    input()