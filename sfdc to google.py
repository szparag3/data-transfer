#! python3

from selenium import webdriver
from modules import sfdc
from oauth2client.service_account import ServiceAccountCredentials
import time, gspread, pprint, datetime, smtplib
from inputs import dump

try:
    print('Colecting data from SFDC')

    sfdcBrowser = sfdc.login()
    sfdcBrowser.find_element_by_link_text("Reports")
    sfdcBrowser.get('https://vmware.my.salesforce.com/00O34000006UBCQ')
    sfdcBrowser.find_element_by_name("run").click()

    for s in range(10):
        time.sleep(1)

    i = 2
    sfdcCase = {}
    while sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({})".format(i)).get_attribute("Class") == "even":
        caseNum = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(1)".format(i)).text
        sfdcCase[caseNum] = {}
        sfdcCase[caseNum]['updated'] = False
        sfdcCase[caseNum]['owner'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(2)".format(i)).text
        sfdcCase[caseNum]['department'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(3)".format(i)).text
        sfdcCase[caseNum]['status'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(4)".format(i)).text
        sfdcCase[caseNum]['subject'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(5)".format(i)).text
        i += 1

    dump = dump.dump    
        

    print('google spreasheet part')


    scope = ['https://spreadsheets.google.com/feeds']

    credentials = ServiceAccountCredentials.from_json_keyfile_name('inputs/gspread-vmw-c79093a07c54.json', scope)

    gc = gspread.authorize(credentials)

    wks = gc.open_by_url('https://docs.google.com/spreadsheets/d/1ZOegpSYLGEDx4huP7lvtaXmI6IhaFv53pNXiUP5jaCo')
    sh = wks.worksheet("BSA request")


    team = {'Carolina Carvajal': 'Carolina','Harini Vasu': 'Harini','Peter Nawrocki': 'Peter'}
    teamReverse = {}

    for key in team:
        teamReverse[team[key]] = key


    googleCase = {}
    closedCases = []
    print('update:')
    empty = 0
    for j in range(2,sh.row_count):
        if sh.cell(j, 1).value in sfdcCase:
            if sh.cell(j, 1).value in dump:
                if sfdcCase[sh.cell(j, 1).value]['status'] != dump[sh.cell(j, 1).value]['status']:
                    print(sh.cell(j, 1).value, 'status')
                    sh.update_cell(j, 2, sfdcCase[sh.cell(j, 1).value]['status'])
                if sfdcCase[sh.cell(j, 1).value]['owner'] != dump[sh.cell(j, 1).value]['owner']:
                    print(sh.cell(j, 1).value, 'owner')
                    sh.update_cell(j, 3, team[sfdcCase[sh.cell(j, 1).value]['owner']])
                if sfdcCase[sh.cell(j, 1).value]['department'] != dump[sh.cell(j, 1).value]['department']:
                    print(sh.cell(j, 1).value, 'department')
                    sh.update_cell(j, 4, sfdcCase[sh.cell(j, 1).value]['department'])
                if sfdcCase[sh.cell(j, 1).value]['subject'] != dump[sh.cell(j, 1).value]['subject']:
                    print(sh.cell(j, 1).value, 'subject')
                    sh.update_cell(j, 5, sfdcCase[sh.cell(j, 1).value]['subject'])
            else:
                print(sh.cell(j, 1).value, 'All')
                sh.update_cell(j, 2, sfdcCase[sh.cell(j, 1).value]['status'])
                sh.update_cell(j, 3, team[sfdcCase[sh.cell(j, 1).value]['owner']])
                sh.update_cell(j, 4, sfdcCase[sh.cell(j, 1).value]['department'])
                sh.update_cell(j, 5, sfdcCase[sh.cell(j, 1).value]['subject'])       
        else:
            closedCases += [sh.cell(j, 1).value]

    print('adding new cases:')
    googleCaseList = sh.col_values(1)
    for sfdcC in sfdcCase:
        if sfdcC not in googleCaseList:
            print(sfdcC)
            sh.insert_row('', index=sh.row_count+1)
            sh.update_cell(sh.row_count, 1, sfdcC)
            sh.update_cell(sh.row_count, 2, sfdcCase[sfdcC]['status'])
            sh.update_cell(sh.row_count, 3, team[sfdcCase[sfdcC]['owner']])
            sh.update_cell(sh.row_count, 4, sfdcCase[sfdcC]['department'])
            sh.update_cell(sh.row_count, 5, sfdcCase[sfdcC]['subject'])
            sh.update_cell(sh.row_count, 6, '???')
            sh.update_cell(sh.row_count, 7, datetime.datetime.now().strftime('%b/%d/%Y'))

    print('deleting closed cases:')
    for close in closedCases:
        print(close)
        sh.delete_row(sh.find(close).row)

    dump = sfdcCase
    print('dump file update I')
    file = open('inputs/dump.py','w',encoding='utf8')
    file.write('dump = ')
    file.write(str(dump))
    file.close()
    print('synch from google to SFDC')



    print('Colecting cases for update')
    googleUpdate = []

    for k in range(2,sh.row_count):
        loopCaseNumber = sh.cell(k, 1).value
        if sh.cell(k, 2).value != dump[loopCaseNumber]['status']:
            googleUpdate += [loopCaseNumber];
            continue
        if sh.cell(k, 3).value != team[dump[loopCaseNumber]['owner']]:
            googleUpdate += [sh.cell(k, 1).value];
            continue
        if sh.cell(k, 4).value != dump[loopCaseNumber]['department']:
            googleUpdate += [loopCaseNumber];
            continue
        if sh.cell(k, 5).value != dump[loopCaseNumber]['subject']:
            googleUpdate += [loopCaseNumber];
            continue

    print('SFDC update')

    for googleC in googleUpdate:
        print(googleC)
        googleRow = sh.find(googleC).row
        sfdc.openCase(sfdcBrowser, googleC)
        for s in range(5):
            time.sleep(1)  
        sfdcBrowser.find_element_by_name("edit").click()
        if sh.cell(googleRow, 2).value != dump[googleC]['status']:
            print('status')
            sfdcBrowser.find_element_by_id("cas7").click()
            sfdcBrowser.find_element_by_css_selector('option[value="{}"]'.format(sh.cell(googleRow, 2).value)).click()
            sfdcBrowser.find_element_by_id("cas7").click()
        if sh.cell(googleRow, 4).value != dump[googleC]['department']:
            print('department')
            sfdcBrowser.find_element_by_id("00N800000051y8j").click()
            sfdcBrowser.find_element_by_css_selector('option[value="{}"]'.format(sh.cell(googleRow, 4).value)).click()
            sfdcBrowser.find_element_by_id("00N800000051y8j").click()
        if sh.cell(googleRow, 5).value != dump[googleC]['subject']:
            print('subject')
            sfdcBrowser.find_element_by_id("cas14").clear()
            sfdcBrowser.find_element_by_id("cas14").send_keys(sh.cell(googleRow, 5).value) 
        sfdcBrowser.find_element_by_name("save").click()
        for s in range(5):
                time.sleep(1)
        if sh.cell(googleRow, 3).value != team[dump[googleC]['owner']]:
            print('owner')
            sfdcBrowser.find_element_by_css_selector("div#cas1_ileinner a:nth-child(2)").click()
            sfdcBrowser.find_element_by_name("newOwn").send_keys(teamReverse[sh.cell(googleRow, 3).value])
            sfdcBrowser.find_element_by_name("save").click()
            for s in range(5):
                time.sleep(1)


    print('Creation final dump')

    sfdcBrowser.find_element_by_link_text("Reports")
    sfdcBrowser.get('https://vmware.my.salesforce.com/00O34000006UBCQ')
    sfdcBrowser.find_element_by_name("run").click()

    for s in range(10):
        time.sleep(1)

    i = 2
    sfdcCase = {}
    while sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({})".format(i)).get_attribute("Class") == "even":
        caseNum = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(1)".format(i)).text
        sfdcCase[caseNum] = {}
        sfdcCase[caseNum]['updated'] = False
        sfdcCase[caseNum]['owner'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(2)".format(i)).text
        sfdcCase[caseNum]['department'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(3)".format(i)).text
        sfdcCase[caseNum]['status'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(4)".format(i)).text
        sfdcCase[caseNum]['subject'] = sfdcBrowser.find_element_by_css_selector("div#fchArea tr:nth-child({}) td:nth-child(5)".format(i)).text
        i += 1

    dump = sfdcCase


    print('dump file update II')
    file = open('inputs/dump.py','w',encoding='utf8')
    file.write('dump = ')
    file.write(str(dump))
    file.close() 
            
    sfdc.closeTheBrowser(sfdcBrowser)

    print('Logs...')
    wksLog = gc.open_by_url('https://docs.google.com/spreadsheets/d/1pI19aUYY83qNfrYp_4bprRxd2wheznpXqTt_Skp8CiA')
    shLog = wksLog.worksheet("logs")
    shLog.add_rows(1)
    shLog.update_cell(shLog.row_count, 1, datetime.datetime.now().strftime('%Y/%b/%d %H:%M'))
    shLog.update_cell(shLog.row_count, 2, 'BSA')
    print('END')      
    
except:
    print('HELP -- ERROR ERROR ERROR')
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()
    smtpobj.login('pnawrocki1983@gmail.com', 'xxxx')
    smtpobj.sendmail('pnawrocki1983@gmail.com', 'szparag3@gmail.com', 'Subject: {0} AUTOMATION ERROR\n{0} AUTOMATION ERROR\nERROR'.format('BSA REQUEST'))
    smtpobj.quit()
    input()
